/*------------------------------------------------------------------------
  Acrobotic - 01/12/2014
  Author: MakerBro (x1sc0@acrobotic.com)
  Platforms: Raspberry Pi
  File: main.js
  ------------------------------------------------------------------------
  Description: 
  This script uses the WebIOPi JS library for interacting with the REST
  API. 
  ------------------------------------------------------------------------
  License:
  TBD
--------------------------------------------------------------------------*/
// For todays date;
Date.prototype.today = function () { 
    return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear();
}

// For the time now:
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}

// Document ready function:
webiopi().ready(function() {
    var content, button;
    content = $("#temp-btn-group");

    // create a button label for the Reset Switch for GPIO 18 (will display state of reset button)
    button = webiopi().createGPIOButton(18, "Reset Switch");
    button.toggleClass("btn btn-info");
    content.append(button); 
    
    // create a button to call the reset range macro
    button = webiopi().createButton("resetRange", "Reset Range", callResetMacro);
    button.toggleClass("btn btn-info");
    content.append(button);
    
    // create a button to call the get temperature macro
    button = webiopi().createButton("getTemp", "Get Temps", callTempMacro);
    button.toggleClass("btn btn-info");
    content.append(button);

    webiopi().refreshGPIO(true);

    // create a temperature display
    var device = webiopi().newDevice("Temperature", "temp0");

    if (device) {
        device.element = $("<div id=\"temp\">");
        $(".panel-heading").append(device.element);
        updateUI(device);
    }
});

function updateUI(device) {
    var temp = device;
    var element = device.element;
    if ((element != undefined) && (element.header == undefined)) {
        element.header = $("<h4>&nbsp;</h4>");
        element.append(element.header);
    }    
    var datetime = new Date().today() + " " + new Date().timeNow();   
    $.get(device.url + "/temperature/c", function(data) {
        if (element != undefined) {
            //console.log(data);
            element.header.text(datetime + " | " + data + "°C");
        }    
    });  

    drawTempChart();

    setTimeout(function(){updateUI(device)}, device.refreshTime);
}

function callResetMacro() {
    // call resetRange
    webiopi().callMacro("ResetTempRange", null);
}

function callTempMacro() {
    // call getTemperature
    webiopi().callMacro("GetSensorTemp", null, callTempMacroCallback);
}

function callTempMacroCallback(macro, args, data) {
    alert(data);
}

function callGetTempLogsMacro() {
    // call GetTempLogs
    webiopi().callMacro("GetTempLogs", 24, callGetTempLogsMacroCallback);
}

function callGetTempLogsMacroCallback(macro, args, data) {
    //buildChart(data);
}
