#--------------------------------------------------------------------------
#  Acrobotic - 01/12/2014
#  Author: MakerBro (x1sc0@acrobotic.com)
#  Platforms: Raspberry Pi
#  File: script.py
#  ------------------------------------------------------------------------
#  Description: 
#
#  ------------------------------------------------------------------------
#  License:
#  TBD
#--------------------------------------------------------------------------
# Imports
import webiopi
from datetime import datetime
import time
from webiopi import deviceInstance
import os,sys
sys.path.append("/home/pi/trt_criollo/python")

# Enable debug output
webiopi.setDebug()

# Retrieve GPIO lib
GPIO = webiopi.GPIO

# Temperature thresholds
COLD = 69.9
HOT = 79.0

# RGB LED GPIO's
RED = 11
GREEN = 9
BLUE = 10

# Switch GPIO
# Next version of WebIOPi should support interrupts (better approach)
SWITCH = 18

# Display State: Current Temperature or Temperature Range
displayCurrent = False

# Set temperature sensor (specified in WebIOPi config)
t = deviceInstance("temp0")

# Initialize temperature range variables
tLow = t.getCelsius()
tHigh = t.getCelsius()

# Hour Counter
hourCounter = datetime.now().hour

# Critical flag
criticalSent = False

root_dir = '/home/pi/trt_criollo'
data_file = root_dir+'/html/data.csv'

# Called by WebIOPi at script loading
def setup():
    webiopi.debug("Script with macros - Setup")
    # Setup GPIOs
    GPIO.setup(SWITCH, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    with open(data_file,'w') as f:
        f.write("date,temp\n")
    

# Looped by WebIOPi
def loop():
    # Run every 5 seconds
    try:
        global tHigh
        global tLow
        global hourCounter
        global criticalSent

        # Get current temperature
        celsius = t.getCelsius()
        webiopi.debug("Current: %0.1f°F" % celsius)

        time = datetime.now().strftime("%d-%b-%y-%H-%M-%S")
        data_str = time+','+str(celsius)+'\n'

        with open(data_file,'a') as f:
            f.write(data_str)

        # Set high & low 
        if celsius > tHigh:
            tHigh = celsius
        if celsius < tLow:
            tLow = celsius
        webiopi.debug("Low:     %0.1f°F" % tLow)
        webiopi.debug("High:    %0.1f°F" % tHigh)

        # Temperature thresholds
        webiopi.debug("Cold Threshold:    %0.1f°F" % COLD)
        webiopi.debug("Hot Threshold:     %0.1f°F" % HOT)
      
        if GPIO.digitalRead(SWITCH) == GPIO.LOW:
            # Reset switch pressed
            tHigh = celsius
            tLow = celsius

        # Log will be uploaded based on the hour counter
        webiopi.debug("Hour Counter:     %0.0f" % hourCounter)
        
        # Check if hour changed
        if hourCounter != datetime.now().hour:
            hourCounter = datetime.now().hour
            # Upload log
            #client = Client(URI)
            #sResult = client.service.UploadLog('Server Room',
            #                                   'Info',
            #                                   celsius)
            #webiopi.debug("Log Upload: " + sResult)
            # Reset critical notification flag
            criticalSent = False
        # Notify if temperature critical
        if not criticalSent and celsius >= HOT:
            #client = Client(URI)
            #sResult = client.service.Notify('rdagger',
            #                                celsius)
            #webiopi.debug("Log Notify: " + sResult)
            # Only allow 1 email per hour
            criticalSent = True
  
    except:
        webiopi.debug("error: " + str(sys.exc_info()[0]))
    finally:
        webiopi.sleep(5)  

# Called by WebIOPi at server shutdown
def destroy():
    webiopi.debug("Script with macros - Destroy")
    # Reset GPIO functions
    GPIO.setFunction(SWITCH, GPIO.IN)

# A macro to reset temperature range from web
@webiopi.macro
def ResetTempRange():
    webiopi.debug("Reset Temp Range Macro...")
    global tHigh
    global tLow
    celsius = t.getCelsius()
    tHigh = celsius
    tLow = celsius

# A macro to get the sensor temperature from web
@webiopi.macro
def GetSensorTemp():
    webiopi.debug("GetSensorTemp Macro...")
    # Get current temperature
    celsius = t.getCelsius()
    return "Current:  %0.1f°F\r\nLow:      %0.1f°F\r\nHigh:     %0.1f°F" % (celsius, tLow, tHigh)

# A macro to get the temperature logs
@webiopi.macro
def GetTempLogs(take):
    webiopi.debug("GetTempLogs Macro...")
    #client = Client(URI)
    #return client.service.GetLog(take)
