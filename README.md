# TRT Criollo #

This repository Contains the code for a Thermal Response Test (TRT) control system running on a Raspberry Pi single-board computer.

## Architecture ##

Refer to the dedicated [Wiki page](https://bitbucket.org/themakerbro/trt_criollo/wiki/Architecture) for details.

## Getting Started ##

### DS18B20 Temperature Sensor ###
- Add the following ling to /boot/config.txt 
```
#!bash
dtoverlay=w1-gpio
```
- Reboot the system
```
#!bash
$ sudo reboot
```
- Issue the following two commands on the terminal:
```
#!bash
$ sudo modprobe w1-gpio
$ sudo modprobe w1-therm
```
- Make sure that the drivers are now loaded by issuing:
```
#!bash
$ lsmod | grep w1
```
Which should return output similar to:
```
#!bash
w1\_therm                3325  0 
w1\_gpio                 4502  0 
wire                   31280  2 w1\_gpio,w1\_therm
```
- Follow the wiring diagram:

![DS18B20 Wiring Diagram](docs/img/wiring_ds18b20.png)

- Run the test python program:
```
#!bash
$ sudo python samples/test\_DS18B20.py
```
Which should return output similar to:
```
#!bash
(24.625, 76.325)
(24.625, 76.325)
```